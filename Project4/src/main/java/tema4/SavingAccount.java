package tema4;

public class SavingAccount extends Account
{
	private double interestRate;

	public SavingAccount(double balance,double interestRate) {
		super(balance,1);
		this.interestRate = interestRate;
	}
	
	public double getInterestRate()
	{
		return interestRate;
	}
	
	public void setInterestRate(double interestRate)
	{
		this.interestRate=interestRate;
	}
	
	public void addInterest()
	{
		this.depozit(this.getBalance()*interestRate/100);
	}
	
	public String toString()
	{
		return super.toString()+" "+interestRate;
	}
}
