package tema4;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import java.awt.event.ActionListener;
import java.util.StringTokenizer;
import java.awt.event.ActionEvent;
import javax.swing.JTable;

public class AccountGUI extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldBalanceA;
	private JTextField textFieldIRA;
	private JTextField textFieldBalanceE;
	private JTextField textFieldIRE;
	private JTextField textFieldSumD;
	private JTextField textFieldSumW;
	private static JComboBox<Person> comboBoxPers;
	private JTable table;

	public static void updatePersons(Person p)
	{
		comboBoxPers.addItem(p);
	}
	
	
	public static void delPersons(Person p)
	{
		comboBoxPers.removeItem(p);
	}
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AccountGUI frame = new AccountGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public AccountGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 1002, 726);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		final JComboBox<String> comboBoxW = new JComboBox<String>();
		comboBoxW.setBounds(174, 201, 223, 20);
		for(Object a:Bank.getBankAccounts().entrySet().toArray())
			comboBoxW.addItem(a.toString());
		contentPane.add(comboBoxW);
		
		final JComboBox<String> comboBoxDep = new JComboBox<String>();
		comboBoxDep.setBounds(174, 157, 223, 20);
		for(Object a:Bank.getBankAccounts().entrySet().toArray())
			comboBoxDep.addItem(a.toString());
		contentPane.add(comboBoxDep);
		
		comboBoxPers = new JComboBox<Person>();
		comboBoxPers.setBounds(691, 23, 285, 20);
		for(Object p:Bank.getPersons().toArray())
		{
			comboBoxPers.addItem(((Person)p));
		}
		contentPane.add(comboBoxPers);
		
		final JComboBox<String> comboBoxType = new JComboBox<String>();
		comboBoxType.setBounds(168, 23, 98, 20);
		comboBoxType.addItem(new String("Saving"));
		comboBoxType.addItem(new String("Spending"));
		contentPane.add(comboBoxType);
		
		final JComboBox<String> comboBoxAccE = new JComboBox<String>();
		comboBoxAccE.setBounds(195, 68, 223, 20);
		for(Object a:Bank.getBankAccounts().entrySet().toArray())
			comboBoxAccE.addItem(a.toString());
		contentPane.add(comboBoxAccE);
		
		JLabel lblAccount = new JLabel("account:");
		lblAccount.setBounds(139, 69, 46, 14);
		contentPane.add(lblAccount);
		
		JLabel label = new JLabel("account:");
		label.setBounds(164, 111, 46, 14);
		contentPane.add(label);
		
		final JComboBox<String> comboBoxAccD = new JComboBox<String>();
		comboBoxAccD.setBounds(220, 110, 223, 20);
		for(Object a:Bank.getBankAccounts().entrySet().toArray())
			comboBoxAccD.addItem(a.toString());
		contentPane.add(comboBoxAccD);
		
		JButton btnNewButton = new JButton("Add account");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String Stype=comboBoxType.getSelectedItem().toString();
				Account a;
				if(Stype.equals("Saving"))
					a=new SavingAccount(Double.parseDouble(textFieldBalanceA.getText()),Double.parseDouble(textFieldIRA.getText()));
				else 
					a=new SpendingAccount(Double.parseDouble(textFieldBalanceA.getText()));
				Person p=((Person)comboBoxPers.getSelectedItem());
				Bank.addAccount(a,p);
				comboBoxAccE.removeAllItems();
				comboBoxAccD.removeAllItems();
				comboBoxDep.removeAllItems();
				comboBoxW.removeAllItems();
				for(Object o:Bank.getBankAccounts().entrySet().toArray())
				{
					comboBoxAccE.addItem(o.toString());
					comboBoxAccD.addItem(o.toString());
					comboBoxDep.addItem(o.toString());
					comboBoxW.addItem(o.toString());
				}
				
			}
		});
		btnNewButton.setBounds(10, 22, 119, 23);
		contentPane.add(btnNewButton);
		
		JLabel lblType = new JLabel("type:");
		lblType.setBounds(139, 26, 46, 14);
		contentPane.add(lblType);
		
		JLabel lblBalance = new JLabel("balance:");
		lblBalance.setBounds(276, 26, 46, 14);
		contentPane.add(lblBalance);
		
		textFieldBalanceA = new JTextField();
		textFieldBalanceA.setBounds(332, 23, 86, 20);
		contentPane.add(textFieldBalanceA);
		textFieldBalanceA.setColumns(10);
		
		JLabel lblIncra = new JLabel("interestRate:");
		lblIncra.setBounds(437, 26, 86, 14);
		contentPane.add(lblIncra);
		
		textFieldIRA = new JTextField();
		textFieldIRA.setBounds(514, 23, 86, 20);
		contentPane.add(textFieldIRA);
		textFieldIRA.setColumns(10);
		
		JLabel lblPerson = new JLabel("person:");
		lblPerson.setBounds(628, 26, 46, 14);
		contentPane.add(lblPerson);
		
		JButton btnNewButton_1 = new JButton("Edit account");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String sel=((String)comboBoxAccE.getSelectedItem());
				StringTokenizer st = new StringTokenizer(sel," ");
		    	String type=st.nextToken();
		        double balance=Double.parseDouble(st.nextToken());
		        Account a=Bank.getAccount(balance);
		        Account a_edit=new SavingAccount(Double.parseDouble(textFieldBalanceE.getText()),Double.parseDouble(textFieldIRE.getText()));
		        Bank.editAccount(a, a_edit);
		        comboBoxAccE.removeAllItems();
				comboBoxAccD.removeAllItems();
				comboBoxDep.removeAllItems();
				comboBoxW.removeAllItems();
		        for(Object o:Bank.getBankAccounts().entrySet().toArray())
				{
					comboBoxAccE.addItem(o.toString());
					comboBoxAccD.addItem(o.toString());
					comboBoxDep.addItem(o.toString());
					comboBoxW.addItem(o.toString());
				}
			}
		});
		btnNewButton_1.setBounds(10, 65, 119, 23);
		contentPane.add(btnNewButton_1);
		
		JButton btnNewButton_2 = new JButton("Delete account");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String sel=((String)comboBoxAccD.getSelectedItem());
				StringTokenizer st = new StringTokenizer(sel," ");
		    	String type=st.nextToken();
		        double balance=Double.parseDouble(st.nextToken());
		        Account a=Bank.getAccount(balance);
		        Bank.removeAccount(a);
		        comboBoxAccE.removeAllItems();
				comboBoxAccD.removeAllItems();
				comboBoxDep.removeAllItems();
				comboBoxW.removeAllItems();
		        for(Object o:Bank.getBankAccounts().entrySet().toArray())
				{
					comboBoxAccE.addItem(o.toString());
					comboBoxAccD.addItem(o.toString());
					comboBoxDep.addItem(o.toString());
					comboBoxW.addItem(o.toString());
				}
			}
		});
		btnNewButton_2.setBounds(10, 110, 132, 23);
		contentPane.add(btnNewButton_2);
		
		JLabel label_1 = new JLabel("balance:");
		label_1.setBounds(443, 71, 46, 14);
		contentPane.add(label_1);
		
		textFieldBalanceE = new JTextField();
		textFieldBalanceE.setColumns(10);
		textFieldBalanceE.setBounds(499, 68, 86, 20);
		contentPane.add(textFieldBalanceE);
		
		JLabel label_2 = new JLabel("interestRate:");
		label_2.setBounds(604, 71, 86, 14);
		contentPane.add(label_2);
		
		textFieldIRE = new JTextField();
		textFieldIRE.setColumns(10);
		textFieldIRE.setBounds(681, 68, 86, 20);
		contentPane.add(textFieldIRE);
		
		JButton btnNewButton_3 = new JButton("Depozit");
		btnNewButton_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String sel=((String)comboBoxDep.getSelectedItem());
				StringTokenizer st = new StringTokenizer(sel," ");
		    	String type=st.nextToken();
		        double balance=Double.parseDouble(st.nextToken());
		        Account a=Bank.getAccount(balance);
		        a.depozit(Double.parseDouble(textFieldSumD.getText()));
		        comboBoxAccE.removeAllItems();
				comboBoxAccD.removeAllItems();
				comboBoxDep.removeAllItems();
				comboBoxW.removeAllItems();
		        for(Object o:Bank.getBankAccounts().entrySet().toArray())
				{
					comboBoxAccE.addItem(o.toString());
					comboBoxAccD.addItem(o.toString());
					comboBoxDep.addItem(o.toString());
					comboBoxW.addItem(o.toString());
				}
			}
		});
		btnNewButton_3.setBounds(10, 154, 89, 23);
		contentPane.add(btnNewButton_3);
		
		JLabel label_3 = new JLabel("account:");
		label_3.setBounds(118, 158, 46, 14);
		contentPane.add(label_3);
		
		JLabel lblSum = new JLabel("sum:");
		lblSum.setBounds(422, 160, 46, 14);
		contentPane.add(lblSum);
		
		textFieldSumD = new JTextField();
		textFieldSumD.setColumns(10);
		textFieldSumD.setBounds(456, 155, 86, 20);
		contentPane.add(textFieldSumD);
		
		
		JButton btnWithdraw = new JButton("Withdraw");
		btnWithdraw.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String sel=((String)comboBoxW.getSelectedItem());
				StringTokenizer st = new StringTokenizer(sel," ");
		    	String type=st.nextToken();
		        double balance=Double.parseDouble(st.nextToken());
		        Account a=Bank.getAccount(balance);
		        a.withdrawal(Double.parseDouble(textFieldSumW.getText()));
		        comboBoxAccE.removeAllItems();
				comboBoxAccD.removeAllItems();
				comboBoxDep.removeAllItems();
				comboBoxW.removeAllItems();
		        for(Object o:Bank.getBankAccounts().entrySet().toArray())
				{
					comboBoxAccE.addItem(o.toString());
					comboBoxAccD.addItem(o.toString());
					comboBoxDep.addItem(o.toString());
					comboBoxW.addItem(o.toString());
				}
			}
		});
		btnWithdraw.setBounds(10, 198, 89, 23);
		contentPane.add(btnWithdraw);
		
		JLabel label_4 = new JLabel("account:");
		label_4.setBounds(118, 202, 46, 14);
		contentPane.add(label_4);

		JLabel label_5 = new JLabel("sum:");
		label_5.setBounds(422, 204, 46, 14);
		contentPane.add(label_5);
		
		textFieldSumW = new JTextField();
		textFieldSumW.setColumns(10);
		textFieldSumW.setBounds(456, 199, 86, 20);
		contentPane.add(textFieldSumW);
		
		JButton btnViewAllAccounts = new JButton("View all accounts");
		btnViewAllAccounts.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					table.setModel(Bank.HashtableModel(Bank.getBankAccounts()));
					
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null,e1.getMessage());
				} 
			}
		});
		btnViewAllAccounts.setBounds(10, 235, 175, 23);
		contentPane.add(btnViewAllAccounts);
		
		table = new JTable();
		table.setBounds(10, 269, 866, 414);
		contentPane.add(table);
	}
}
