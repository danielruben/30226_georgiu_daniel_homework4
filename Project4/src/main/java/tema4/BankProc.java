package tema4;

public interface BankProc{
	
	public void addPerson(Person p);
	
	public void removePerson(Person p);
	
	public void editPerson(Person p,Person p_edit);
	
	public void addAccount(Account a,Person p);
	
	public void removeAccount(Account a);
	
	public void editAccount(Account a,Account a_edit);
	
	public void loadData();
	
	public void updateData();
}
