package tema4;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MainGUI {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainGUI window = new MainGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 284, 356);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton btnNewButton = new JButton("Person Operations");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try
				{
					Bank.loadData();
					PersonGUI p=new PersonGUI();
					p.setVisible(true);
				}
				catch(Exception e1)
				{
					JOptionPane.showMessageDialog(null,e1.getMessage());
				}
			}
		});
		btnNewButton.setBounds(48, 55, 157, 96);
		frame.getContentPane().add(btnNewButton);
		
		JButton btnAccountsOperations = new JButton("Account Operations");
		btnAccountsOperations.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try
				{
					Bank.loadData();
					AccountGUI p=new AccountGUI();
					p.setVisible(true);
				}
				catch(Exception e2)
				{
					JOptionPane.showMessageDialog(null,e2.getMessage());
				}
			}
		});
		btnAccountsOperations.setBounds(48, 184, 157, 96);
		frame.getContentPane().add(btnAccountsOperations);
	}
}
