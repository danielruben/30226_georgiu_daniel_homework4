package tema4;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;


import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.HashSet;
import java.util.Vector;
import java.awt.event.ActionEvent;

public class PersonGUI extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldIdA;
	private JTextField textFieldNameA;
	private JTextField textFieldEmailA;
	private JTextField textFieldTelefonA;
	private JTextField textFieldIdE;
	private JTextField textFieldNameE;
	private JTextField textFieldEmailE;
	private JTextField textFieldTelefonE;
	private JTable table;
	private JTextField textFieldIdD;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PersonGUI frame = new PersonGUI();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public PersonGUI() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 670, 463);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnNewButton = new JButton("Add person");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				int id=Integer.parseInt(textFieldIdA.getText());
				Person p=new Person(id,textFieldNameA.getText(),textFieldEmailA.getText(),textFieldTelefonA.getText());
				Bank.addPerson(p);
				AccountGUI.updatePersons(p);
			}
		});
		btnNewButton.setBounds(10, 28, 89, 23);
		contentPane.add(btnNewButton);
		
		JLabel lblId = new JLabel("id:");
		lblId.setBounds(112, 32, 46, 14);
		contentPane.add(lblId);
		
		textFieldIdA = new JTextField();
		textFieldIdA.setBounds(139, 29, 74, 20);
		contentPane.add(textFieldIdA);
		textFieldIdA.setColumns(10);
		
		JLabel lblName = new JLabel("name:");
		lblName.setBounds(223, 32, 46, 14);
		contentPane.add(lblName);
		
		textFieldNameA = new JTextField();
		textFieldNameA.setBounds(261, 29, 86, 20);
		contentPane.add(textFieldNameA);
		textFieldNameA.setColumns(10);
		
		JLabel lblEmail = new JLabel("email:");
		lblEmail.setBounds(357, 32, 46, 14);
		contentPane.add(lblEmail);
		
		textFieldEmailA = new JTextField();
		textFieldEmailA.setBounds(388, 29, 86, 20);
		contentPane.add(textFieldEmailA);
		textFieldEmailA.setColumns(10);
		
		JLabel lblTelefon = new JLabel("telefon:");
		lblTelefon.setBounds(489, 32, 46, 14);
		contentPane.add(lblTelefon);
		
		textFieldTelefonA = new JTextField();
		textFieldTelefonA.setBounds(531, 29, 86, 20);
		contentPane.add(textFieldTelefonA);
		textFieldTelefonA.setColumns(10);
		
		JButton btnEditPerson = new JButton("Edit person");
		btnEditPerson.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e){
				int id=Integer.parseInt(textFieldIdE.getText());
				String name=textFieldNameE.getText();
				String email=textFieldEmailE.getText();
				String telefon=textFieldTelefonE.getText();
				Person p=Bank.getPersonById(id);
				Person p_edit=new Person(id,name,email,telefon);
				Bank.editPerson(p, p_edit);
			}
		});
		btnEditPerson.setBounds(10, 75, 89, 23);
		contentPane.add(btnEditPerson);
		
		JLabel label = new JLabel("id:");
		label.setBounds(112, 79, 46, 14);
		contentPane.add(label);
		
		textFieldIdE = new JTextField();
		textFieldIdE.setColumns(10);
		textFieldIdE.setBounds(139, 76, 74, 20);
		contentPane.add(textFieldIdE);
		
		JLabel label_1 = new JLabel("name:");
		label_1.setBounds(223, 79, 46, 14);
		contentPane.add(label_1);
		
		textFieldNameE = new JTextField();
		textFieldNameE.setColumns(10);
		textFieldNameE.setBounds(261, 76, 86, 20);
		contentPane.add(textFieldNameE);
		
		textFieldEmailE = new JTextField();
		textFieldEmailE.setColumns(10);
		textFieldEmailE.setBounds(388, 76, 86, 20);
		contentPane.add(textFieldEmailE);
		
		JLabel label_2 = new JLabel("email:");
		label_2.setBounds(357, 79, 46, 14);
		contentPane.add(label_2);
		
		JLabel label_3 = new JLabel("telefon:");
		label_3.setBounds(489, 79, 46, 14);
		contentPane.add(label_3);
		
		textFieldTelefonE = new JTextField();
		textFieldTelefonE.setColumns(10);
		textFieldTelefonE.setBounds(531, 76, 86, 20);
		contentPane.add(textFieldTelefonE);
		
		JButton btnDeletePerson = new JButton("Delete person");
		btnDeletePerson.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int id=Integer.parseInt(textFieldIdD.getText());
				Person p=Bank.getPersonById(id);
				Bank.removePerson(p);
				AccountGUI.delPersons(p);
			}
		});
		btnDeletePerson.setBounds(10, 117, 124, 23);
		contentPane.add(btnDeletePerson);
		
		JButton btnView = new JButton("View all persons");
		btnView.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					table.setModel(Bank.HashSetModel(Bank.getPersons()));
					
				} catch (Exception e1) {
					JOptionPane.showMessageDialog(null,e1.getMessage());
				} 
			}
		});
		btnView.setBounds(10, 155, 159, 23);
		contentPane.add(btnView);
		
		table = new JTable();
		table.setBounds(10, 191, 634, 233);
		contentPane.add(table);
		
		JLabel label_4 = new JLabel("id:");
		label_4.setBounds(150, 123, 46, 14);
		contentPane.add(label_4);
		
		textFieldIdD = new JTextField();
		textFieldIdD.setColumns(10);
		textFieldIdD.setBounds(177, 120, 74, 20);
		contentPane.add(textFieldIdD);
	}
}
