package tema4;

public class Person{
	
	private int id;
	private String name;
	private String email;
	private String telefon;
	
	public Person(int id, String name, String email, String telefon) {
		this.id = id;
		this.name = name;
		this.email = email;
		this.telefon = telefon;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefon() {
		return telefon;
	}

	public void setTelefon(String telefon) {
		this.telefon = telefon;
	}
	
	public int hashCode()
	{
		return id;
	}
	
	public boolean equals(Object o)
	{
		if(((Person)o).getId()==id)
			return true;
		return false;
	}
	
	public String toString()
	{
		return id+" "+name+" "+email+" "+telefon;
	}
}
