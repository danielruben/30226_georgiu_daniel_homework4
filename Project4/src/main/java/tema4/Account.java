package tema4;

public class Account
{
	private double balance;
	public int type;
	
	public Account()
	{
		this.balance=0;
	}
	
	public Account(double initBalance,int type)
	{
		this.balance=initBalance;
		this.type=type;
	}
	
	public double getBalance()
	{
		return balance;
	}
	
	public int getType()
	{
		return type;
	}
	
	public void setBalance(double balance)
	{
		this.balance=balance;
	}
	
	public void depozit(double sum)
	{
		balance+=sum;
		Bank.updateData();
	}
	
	public boolean withdrawal(double sum)
	{
		if(balance>=sum)
		{
			balance-=sum;
			Bank.updateData();
			return true;
		}
		return false;
	}
	
	public int hashCode()
	{
		return type+(int)balance;
	}
	
	public boolean equals(Object o)
	{
		if(((Account)o).hashCode()==this.hashCode())
			return true;
		return false;
	}
	
	public String toString()
	{
		return type+" "+balance;
	}
}
	
