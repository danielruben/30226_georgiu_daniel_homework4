package tema4;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.PrintWriter;
import java.sql.ResultSetMetaData;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Vector;

import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

public class Bank
{
	private static HashSet<Person> persons=new HashSet<Person>();
	private static Hashtable<Account,Person> bankAccounts=new Hashtable<Account,Person>();
	
	public Bank(){
		
	}
	
	public static Hashtable<Account,Person> getBankAccounts()
	{
		return bankAccounts;
	}
	
	public static HashSet<Person> getPersons()
	{
		return persons;
	}
	
	public static void addPerson(Person p)
	{
		persons.add(p);
	}
	
	public static void removePerson(Person p)
	{
		persons.remove(p);
		if(bankAccounts.contains(p))
		{
			for (Enumeration<Account> e=bankAccounts.keys(); e.hasMoreElements();)
			       bankAccounts.remove(e.nextElement(), p);
		}
		Bank.updateData();
	}
	
	public static void editPerson(Person p,Person p_edit)
	{
		if(p_edit.getEmail().length()!=0)
			p.setEmail(p_edit.getEmail());
		if(p_edit.getName().length()!=0)
			p.setName(p_edit.getName());
		if(p_edit.getTelefon().length()!=0)
			p.setTelefon(p_edit.getTelefon());
		Bank.updateData();
	}
	
	public static Person getPersonById(int id)
	{
		for(Object p:persons.toArray())
		{
			if(((Person)p).getId()==id)
				return (Person)p;
		}
		return null;
	}
	
	public static Account getAccount(double balance)
	{
		for(Object a:bankAccounts.keySet())
		{
			if(((Account)a).getBalance()==balance)
			{
				return ((Account)a);
			}
		}
		return null;
	}
	
	public static void addAccount(Account a,Person p)
	{
		persons.add(p);
		bankAccounts.put(a, p);
		Bank.updateData();
	}
	
	public static void removeAccount(Account a)
	{
		bankAccounts.remove(a);
		Bank.updateData();
	}
	
	public static void editAccount(Account a,Account a_edit)
	{
		//Person p=bankAccounts.get(a);
		//bankAccounts.remove(a);
		a.setBalance(a_edit.getBalance());
		if(a.getType()==1)
			((SavingAccount)a).setInterestRate(((SavingAccount)a_edit).getInterestRate());
		//bankAccounts.put(a,p);
		Bank.updateData();
	}
	
	public static void loadData()
	{
		persons.clear();
		bankAccounts.clear();
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader("bankData.txt"));
		    
			String line = br.readLine();
		    String person[]=new String[5];
		    String account[]=new String[3];
            
		    while (line != null) {
		    	StringTokenizer st = new StringTokenizer(line," [=]");
		    	account[0]=st.nextToken();
		        int type=Integer.parseInt(account[0]);
		        Account a;
		        if(type==1)
		        {
		        	account[1]=st.nextToken();
		        	account[2]=st.nextToken();
		        	a=new SavingAccount(Double.parseDouble(account[1]),Double.parseDouble(account[2]));
		        }
		        else
		        {
		        	account[1]=st.nextToken();
		        	account[2]=st.nextToken();
		        	a=new SpendingAccount(Double.parseDouble(account[1]));
		        }
		        for(int i=0;i<5;i++)
		        {
		        	person[i]=st.nextToken();
		        }
		        Person p=new Person(Integer.parseInt(person[0]),person[1]+" "+person[2],person[3],person[4]);
		        Bank.addAccount(a, p);
		        line = br.readLine();
		    }
		    br.close();
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
	}
	
	public static void updateData()
	{
		try{
		    PrintWriter writer = new PrintWriter("bankData.txt", "UTF-8");
		    String data=Bank.bankAccounts.entrySet().toString();
		    StringTokenizer st = new StringTokenizer(data,",");
		    while (st.hasMoreTokens()){
		    	writer.println(st.nextToken());
		    }
		    writer.close();
		} 
		catch (Exception e)
		{
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
	}
	
	public static TableModel HashSetModel(HashSet<Person> persons) {
		try {
		    int numberOfColumns =4;
		    Vector<String> columnNames= new Vector<String>();;

		    // Get the column names
			columnNames.addElement("id");
			columnNames.addElement("name");
			columnNames.addElement("email");
			columnNames.addElement("telefon");
			
		    // Get all rows.
		    Vector<Vector<Object>> rows = new Vector<Vector<Object>>();
		    
		    Vector<Object> fRow = new Vector<Object>();
	    	fRow.addElement("id");
	    	fRow.addElement("name");
	    	fRow.addElement("email");
	    	fRow.addElement("telefon");
	    	
	    	rows.addElement(fRow);

		    for(Object p:persons.toArray())
		    {
		    	Vector<Object> newRow = new Vector<Object>();
		    	Person p1=(Person)p;
		    	newRow.addElement(p1.getId());
		    	newRow.addElement(p1.getName());
		    	newRow.addElement(p1.getEmail());
		    	newRow.addElement(p1.getTelefon());
		    	
		    	rows.addElement(newRow);
		    }

		    return new DefaultTableModel(rows, columnNames);
		} catch (Exception e) {
		    e.printStackTrace();

		    return null;
		}
	}
	public static TableModel HashtableModel(Hashtable<Account,Person> bankAccounts) {
		try {
		    int numberOfColumns =7;
		    Vector<String> columnNames= new Vector<String>();;

		    // Get the column names
		    columnNames.addElement("type");
			columnNames.addElement("balance");
			columnNames.addElement("interestRate");
			columnNames.addElement("id");
			columnNames.addElement("name");
			columnNames.addElement("email");
			columnNames.addElement("telefon");
			
		    // Get all rows.
		    Vector<Vector<Object>> rows = new Vector<Vector<Object>>();
		    
		    Vector<Object> fRow = new Vector<Object>();
		    fRow.addElement("type");
			fRow.addElement("balance");
			fRow.addElement("interestRate");
	    	fRow.addElement("id");
	    	fRow.addElement("name");
	    	fRow.addElement("email");
	    	fRow.addElement("telefon");
	    	
	    	rows.addElement(fRow);

		    for(Object o:bankAccounts.entrySet().toArray())
		    {
		    	String person[]=new String[5];
				String account[]=new String[3];
		    	Vector<Object> newRow = new Vector<Object>();
		    	String line=o.toString();
		    	StringTokenizer st = new StringTokenizer(line," =");
		    	account[0]=st.nextToken();
		        account[1]=st.nextToken();
		        account[2]=st.nextToken();
		        for(int i=0;i<5;i++)
		        {
		        	person[i]=st.nextToken();
		        }
		    	newRow.addElement(account[0]);
		    	newRow.addElement(account[1]);
		    	newRow.addElement(account[2]);
		    	newRow.addElement(person[0]);
		    	newRow.addElement(person[1]+person[2]);
		    	newRow.addElement(person[3]);
		    	newRow.addElement(person[4]);
		    	
		    	rows.addElement(newRow);
		    }

		    return new DefaultTableModel(rows, columnNames);
		} catch (Exception e) {
		    e.printStackTrace();

		    return null;
		}
	}
	
	public static void main(String args[])
	{
		/*Person p1=new Person(1256,"Popescu Ion","popescuion@gmail.com","0753534674");
		Person p2=new Person(5743,"Popescu Vasile","popescuVasile@gmail.com","0616882874");
		Person p2_edit=new Person(1256,"Popescu George","popescuge@gmail.com","5673463635");
		SavingAccount a1=new SavingAccount(1245.3,2.6);
		SavingAccount a2=new SavingAccount(4565.6,1.9);
		SavingAccount a3=new SavingAccount(34543.57,4.3);
		SavingAccount a3_edit=new SavingAccount(13424,5.8);
		b.addAccount(a1,p1);
		b.addAccount(a2, p2);
		b.addAccount(a3, p1);
		b.removeAccount(a1);
		System.out.println(b.getPersons().toString());
		System.out.println(b.getBankAccounts());
		b.editAccount(a2, a3_edit);
		System.out.println(a2.getBalance()+"   "+a2.getInterestRate());
		System.out.println(b.getBankAccounts());
		b.removePerson(p2);
		System.out.println(b.getBankAccounts());
		b.editPerson(p1, p2_edit);
		System.out.println(b.getBankAccounts().get(a3).getEmail());
		a3.depozit(10000);
		System.out.println(a3.getBalance());*/
		
		Bank.loadData();
		for(Object p:Bank.getPersons().toArray())
		{
			System.out.println(((Person)p).toString());
		}
		for (Enumeration<Account> e=Bank.getBankAccounts().keys(); e.hasMoreElements();)
		       System.out.println(e.nextElement().toString());
		for (Enumeration<Person> e=Bank.getBankAccounts().elements(); e.hasMoreElements();)
		       System.out.println(e.nextElement().toString());
		System.out.println(Bank.bankAccounts.entrySet().toString());
		SavingAccount a1=new SavingAccount(7565.6,1.9);
		Person p2=new Person(5743,"Popescu Vasile","popescuVasile@gmail.com","0616882874");
		//Bank.addAccount(a1, p2);
		Bank.removeAccount(a1);
		Bank.updateData();
		 for(Object o:bankAccounts.entrySet().toArray())
		 {
			 System.out.println(o.toString());
		 }
	}
}
