package tema4;

public class SpendingAccount extends Account
{
	private int transactionCount;

	public SpendingAccount(double balance) {
		super(balance,0);
		this.transactionCount =0;
	}
	
	public int  getTransactionCount()
	{
		return transactionCount;
	}
	
	public void depozit(double sum)
	{
		super.depozit(sum);
		transactionCount++;
	}
	
	public boolean withdrawal(double sum)
	{
		transactionCount++;
		return super.withdrawal(sum);
	}
	
	public String toString()
	{
		return super.toString()+" "+transactionCount;
	}
}
